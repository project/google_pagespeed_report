<?php

/**
 * @file
 * Contains Drupal\google_pagespeed_report\Form\GetPageSpeedForm.
 */
namespace Drupal\google_pagespeed_report\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class PageSpeedConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'google_pagespeed.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'Google Page Speed setting';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_pagespeed.adminsettings');

    $form['google_pagespeed_data_refresh_time'] = [
      '#type' => 'number',
      '#title' => $this->t('Data Refresh Time'),
      '#description' => $this->t('Google PageSpeed data refresh timing.'),
      '#default_value' => $config->get('google_pagespeed_data_refresh_time') ? $config->get('google_pagespeed_data_refresh_time') : 86400,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $data_refresh_time = $form_state->getValue('google_pagespeed_data_refresh_time') ? $form_state->getValue('google_pagespeed_data_refresh_time') : 86400;

    $this->config('google_pagespeed.adminsettings')
      ->set('google_pagespeed_data_refresh_time', $data_refresh_time)
      ->save();
  }

}
